import Provider from './Provider';
import I18n, { Context, Consumer } from './I18n';

export default I18n;
export { Context, Provider, Consumer };
export { default as i18nBabelPlugin } from './tools/babel-plugin';
